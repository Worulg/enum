/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

/**
 *
 * @author pierrea
 */
public class GestionVolume {
     
    public Volume getVolumeAct() {

        return volumeAct;

    }

    public void setVolumeAct(Volume volumeAct) {

        this.volumeAct = volumeAct;

    }

    public enum Volume {
        
        Min("min"),

        Low("bas"),

        Medium("moyen"),

        High("haut"),
        
        Max("max");
        
        private String strVol = "";
        
        Volume(String strVol) {
        this.strVol = strVol;
        }
        public String toString() {
            return strVol;
        }
    }
    
   
    
    private Volume volumeAct;

 

    public GestionVolume(Volume volumeDepart) {

        volumeAct = volumeDepart;

    }


    public void Monter() {

        switch (getVolumeAct()) {
            
            case Min:

                setVolumeAct(Volume.Low);

                break;

            case Low:

                setVolumeAct(Volume.Medium);

                break;

            case Medium:

                setVolumeAct(Volume.High);

                break;
                
            case High:

                setVolumeAct(Volume.Max);

                break;

        }

    }

    public void Descendre() {

        switch (getVolumeAct()) {
                        
             case Low:

                setVolumeAct(Volume.Min);

                break;
            
            case Medium:

                setVolumeAct(Volume.Low);

                break;

            case High:

                setVolumeAct(Volume.Medium);

                break;
                
            
            case Max:

                setVolumeAct(Volume.High);

                break;

        }

    }
}

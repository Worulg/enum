/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

/**
 *
 * @author pierrea
 */
public class GestionMode {

    public Mode getModeAct() {

        return modeAct;

    }

    public void setModeAct(Mode modeAct) {

        this.modeAct = modeAct;

    }

    public enum Mode {

        off("off"),
        silence("silence"),
        on("on");

        private String strMode = "";

        Mode(String strMode) {
            this.strMode = strMode;
        }

        public String toString() {
            return strMode;
        }
    }

    private Mode modeAct;

    public GestionMode(Mode modeDepart) {

        modeAct = modeDepart;

    }

    public void Changer() {

        switch (getModeAct()) {

            case off:

                setModeAct(Mode.silence);

                break;

            case silence:

                setModeAct(Mode.on);

                break;

            case on:

                setModeAct(Mode.off);

                break;

        }

    }

}
